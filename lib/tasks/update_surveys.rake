namespace :update_surveys do
  desc "TODO"
  task reset_all: :environment do

  	Survey.delete_all
  	
  	surveys=YAML.load_file(File.join Rails.root,'lib/assets/survey.yml')

 	surveys.each do |s|
 		# Create Survey
 		new_survey = Survey.create
 		s["questions"].each do |q|
 			section_name = q["question"]["section_name"]
 			title = q["question"]["title"]
 			body = q["question"]["body"]
 			answer_display = q["question"]["answer_display"]
 			# Create Question
 			new_question = Question.create({
 				section_name: section_name, 
 				title: title,
 				body: body,
 				answer_display: answer_display,
 				survey_id: new_survey.id})
 			options = q["question"]["options"]
 			if options
	 			options.each do |o|
	 				option_kind = o["option"]["kind"]
	 				option_body = o["option"]["body"]
	 				# Create Option
	 				new_option = Option.create({
	 					kind: option_kind,
	 					body: option_body,
	 					question_id: new_question.id
	 					})

 				end # end options
 			end
 		end # end questions
 	end # end surveys

  end # end task reset_all

end
