
Rails.application.routes.draw do
  # resources :surveys
  resources :encounters
  resources :responses
  resources :questions, only: :show

	get '/', to: 'dashboard#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
