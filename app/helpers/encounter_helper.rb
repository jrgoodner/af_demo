module EncounterHelper

	def returnSectionStatus(section_name,encounter,survey_id)
		status = Response.section_status(section_name,encounter,survey_id)
		case status
		when 0
			return {:icon => '<i class="material-icons red">cancel</i>',
					:status_name => "Not Started"}
		when 1
			return {:icon => '<i class="material-icons yellow">do_not_disturb_on</i>',
					:status_name => "Incomplete"}
		when 2
			return {:icon => '<i class="material-icons green">check</i>',
					:status_name => "Complete"}
		end

	end

end
