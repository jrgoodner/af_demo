class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # FIXME not secure, just done for demo purposes
  skip_before_action :verify_authenticity_token #, if: -> { request.xhr? }

end
