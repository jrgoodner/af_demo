class DashboardController < ApplicationController

	layout 'dashboard'

	def index
		@encounters = Encounter.all
		@survey = Survey.first
	end
end
