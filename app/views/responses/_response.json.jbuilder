json.extract! response, :id, :created_at, :updated_at, :question_id, :body
json.url response_url(response, format: :json)
