json.sections %w{A B C D E F} do |section|

	json.name section
	json.status Response.section_status(section,@encounter,@survey.id)

end