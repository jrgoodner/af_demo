$(function () { 

    // Trend modal
    $('a.response-trend').click(function(){

        // Update trend modal content
        var questionId = $(this).data("question-id");

        $.get( "/questions/" + questionId + ".json", function( data ) {
            debugger
            console.log(data);
            questionTitle = data.answer_display;

            var now = moment();

            // Add plot
            Highcharts.chart('plot-container', {
                chart: {
                    type: 'line',
                    backgroundColor: '#fafafa'
                },
                title: {
                    text: questionTitle
                },

                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    color: '#5E67E9',
                    showInLegend: false,
                    data: [
                        [now.subtract(11*3,'Hours').valueOf(),9],
                        [now.add(3,'Hours').valueOf(),7],
                        [now.add(3,'Hours').valueOf(),6],
                        [now.add(3,'Hours').valueOf(),5],
                        [now.add(3,'Hours').valueOf(),6],
                        [now.add(3,'Hours').valueOf(),9],
                        [now.add(3,'Hours').valueOf(),9],
                        [now.add(3,'Hours').valueOf(),7],
                        [now.add(3,'Hours').valueOf(),6],
                        [now.add(3,'Hours').valueOf(),4],
                        [now.add(3,'Hours').valueOf(),3]]
                }]
            });      

            // Open modal
            $('#trend-modal').modal("open");

        }); // end .get
    });

    var now = moment();

    // dashboard trend line
    Highcharts.chart('compliance-trend-container', {
        chart: {
            height: 300,
            type: 'line'
        },

        title: {
            text: ''
        },

        // subtitle: {
        //     text: 'Source: thesolarfoundation.com'
        // },

        yAxis: {
            title: {
                text: '% Complete'
            }
        },
        xAxis: {
            type:'datetime',
            labels: {
              format: '{value:%b %e}'
            }
        },        
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        series: [{
            color: '#5E67E9',
            showInLegend: false,
            data: [
                [now.subtract(11*12,'hours').valueOf(),90],
                [now.add(12,'Hours').valueOf(),75],
                [now.add(12,'Hours').valueOf(),61],
                [now.add(12,'Hours').valueOf(),54],
                [now.add(12,'Hours').valueOf(),60],
                [now.add(12,'Hours').valueOf(),95],
                [now.add(12,'Hours').valueOf(),94],
                [now.add(12,'Hours').valueOf(),77],
                [now.add(12,'Hours').valueOf(),65],
                [now.add(12,'Hours').valueOf(),43],
                [now.add(12,'Hours').valueOf(),31]]
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });




});