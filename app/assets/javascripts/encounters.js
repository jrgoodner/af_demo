$(document).ready(function(){

    // Run on window load

    $('.collapsible').collapsible();
    $('.modal').modal({});

    // at window load, if user is not logged in, open modal
    if (isUserLoggedIn() == "0") {
        $('#identity-modal').modal("open");
    }

    // Set timer for logout
    resetLoginTimeout();

    // Check for logout every 3 seconds and open modal if true
    setInterval(function(){
        console.log("checking login");
        if (isUserLoggedIn() == "0") {
            $('#identity-modal').modal("open");
        }
    },1000*3);

    // When modal button clicked, log in user
    $("#login-as-user").click(function(){
        console.log("logging in " + $("#user-name").html());
        logInUser();
    })    

    // Autosave survey responses
	$( "form.response input" ).change(function() {
        // reset login timeOut
        resetLoginTimeout();
        // Obtain name for section in order to pull section status
        $section = $(this).parents("li");
        sectionName = $section.data("section-name");
        // Submit data
		console.log( "Handler for .change() called." );
		$.ajax({
            url     : $(this.closest("form")).attr('action'),
            type    : $(this.closest("form")).attr('method'),
            dataType: 'json',
            data    : $(this.closest("form")).serialize(),
            success : function( data ) {
					  	console.log("form submitted. Data:");
					  	console.log(data);
                        // Update status
                        // Request encounter statuses
                        $.get( "/encounters/" + $("#encounter-id").html() + ".json", function( data ) {
                          console.log(data);
                          var status = data.sections.find(function(pair) {return pair.name == sectionName}).status;
                          // Update section status
                          updateSectionStatus($section,status);
                        });

                      },
            error   : function( xhr, err ) {
                        console.log('Error');     
                      }
    	});   
	});


});

// Variables
var saveCount = 0;
var timeOut;

// Functions

function isUserLoggedIn() {
    if (localStorage.getItem("logged-in") == null) {
        localStorage.setItem("logged-in", 0);
        return 0;
    }
    else {
        return localStorage.getItem("logged-in")
    }
}

function logInUser() {
    localStorage.setItem("logged-in", 1);
    localStorage.setItem("user-last-logged-in", $("#user-name").html());
    console.log("Logged in user");
    resetLoginTimeout();
}    

function logOutUser() {
    console.log("Logged out user");
    localStorage.setItem("logged-in", 0);
}        

function resetLoginTimeout() {
    // Log user out after 1 minute
    clearTimeout(timeOut);
    console.log("resetting timeout")
    timeOut = setTimeout(function(){
        logOutUser();
        $('#identity-modal').modal("open");
    }, 1000*60*1);    
}

function updateSectionStatus($section,statusValue) {
    // set icon
    switch(statusValue) {
        case 0:
            var iconHtml = '<i class="material-icons red">cancel</i>';
            var statusName = "Not Started";
            break;
        case 1:
            var iconHtml = '<i class="material-icons yellow">do_not_disturb_on</i>';
            var statusName = "Incomplete";
            break;
        case 2:
            var iconHtml = '<i class="material-icons green">check</i>';
            var statusName = "Complete";
            break;
    }
    $($section).find(".status-icon").html(iconHtml);
    $($section).find(".status-name").html(statusName);

    return;

}

