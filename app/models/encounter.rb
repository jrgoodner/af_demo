class Encounter < ApplicationRecord

	has_many :responses

	before_create :generate_mrn

	def get_response(question_id)
		responses = self.responses.where("question_id = #{question_id}")
		if responses.empty?
			# create a response and return it if question exists
			if Question.find(question_id)
				Response.create(encounter_id: self.id, question_id: question_id)
			else
				nil
			end
		else
			# return first response that matches with question id
			responses.first
		end
	end

	def generate_mrn 
		self.mrn = (rand*1000000).to_i
	end

end
