class Response < ApplicationRecord
	belongs_to :question
	belongs_to :encounter

	# Return status of completion of a specified section of a survey
	def self.section_status(section_name,encounter_id,survey_id)

		# Obtain questions in section
		questions = Survey.find(survey_id).questions.group_by {|q| q.section_name}
		section_questions = questions[section_name] || []

		# Obtain all encounter responses
		encounter = Encounter.find(encounter_id)
		responses = encounter.responses
		
		# Check for encounter responses for each question in desired section
		response_count = 0
		section_questions.each do |q|
			question_responses = responses.where(question_id: q.id).where.not(body: nil)
			response_count += question_responses.length
		end

		if response_count < 1
			# not started
			0
		elsif response_count < section_questions.length
			# incomplete
			1
		else
			# complete
			2
		end

	end
	
end
