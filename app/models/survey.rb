class Survey < ApplicationRecord

	has_many :questions
	has_many :responses, through: :question

	SECTIONS = %w{A B C D E F}
	
end
