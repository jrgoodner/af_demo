class Subject < ApplicationRecord
	has_many: :responses
	has_many: :encounters

end
