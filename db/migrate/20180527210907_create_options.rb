class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
      t.string :kind
      t.string :body
      t.integer :question_id

      t.timestamps
    end
  end
end
