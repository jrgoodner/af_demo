class CreateResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :responses do |t|
      t.integer :question_id
      t.string :body
      t.integer :user_id
      t.integer :encounter_id

      t.timestamps
    end
  end
end
