class CreateSubjects < ActiveRecord::Migration[5.0]
  def change
    create_table :subjects do |t|
      t.string :first_name
      t.string :last_name
      t.string :physician
      t.string :admitted
      t.string :bed
      t.string :mrn

      t.timestamps
    end
  end
end
