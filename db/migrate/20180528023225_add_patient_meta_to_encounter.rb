class AddPatientMetaToEncounter < ActiveRecord::Migration[5.0]
  def change
    change_table :encounters do |t|
		t.string :subject_name
		t.string :physician
		t.string :bed
		t.string :mrn
    end

  end
end
