class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|

      t.integer :order
      t.string :section_name
      t.string :title
      t.string :body
      t.string :answer_display
      t.integer :survey_id

      t.timestamps
    end
  end
end
